package cool.taomu.mysql.entity

import java.sql.Timestamp
import org.eclipse.xtend.lib.annotations.Accessors
import org.eclipse.xtend.lib.annotations.ToString

@Accessors
@ToString
class DatabaseTaskEntity {
	String uuid;
	String script;
	Timestamp updateDatetime;
	int state; //1 已执行 0 未执行 -1 弃用
	boolean timerTask; // true 定时任务 
	boolean tempTask; //true 临时任务，执行后第二天删除 
}
