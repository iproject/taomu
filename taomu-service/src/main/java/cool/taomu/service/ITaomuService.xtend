package cool.taomu.service

import com.google.inject.Injector

interface ITaomuService {
	def void start(Injector injector);
	def void stop();
}