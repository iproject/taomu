package cool.taomu.pool

import java.util.concurrent.atomic.AtomicLong
import org.apache.commons.pool2.BaseKeyedPooledObjectFactory
import org.apache.commons.pool2.KeyedObjectPool
import org.apache.commons.pool2.PooledObject
import org.apache.commons.pool2.impl.DefaultPooledObject
import org.apache.commons.pool2.impl.GenericKeyedObjectPool
import org.apache.commons.pool2.impl.GenericKeyedObjectPoolConfig
import org.eclipse.xtend.lib.annotations.Accessors
import org.slf4j.LoggerFactory
import cool.taomu.pool.KeyedObjectPoolUtils.ObjectPoolWrapper

@Accessors
class KeyedObjectPoolFactory<T> extends BaseKeyedPooledObjectFactory<String, ObjectPoolWrapper<T>> {
	val static LOG = LoggerFactory.getLogger(KeyedObjectPoolFactory);
	var KeyedObjectPool<String, ObjectPoolWrapper<T>> pool;
	var ClassLoader loader = null;
	var AtomicLong id = new AtomicLong(0);

	override create(String key) throws Exception {
		if (loader !== null) {
			var zlass = loader.loadClass(key);
			var T instance = zlass.newInstance as T;
			return new ObjectPoolWrapper(pool, instance, id.incrementAndGet);
		}
	}

	override wrap(ObjectPoolWrapper<T> value) {
		return new DefaultPooledObject<ObjectPoolWrapper<T>>(value);
	}

	override PooledObject<ObjectPoolWrapper<T>> makeObject(String key) throws Exception {
		LOG.info("创建对象:" + key)
		return super.makeObject(key);
	}

	override activateObject(String key, PooledObject<ObjectPoolWrapper<T>> p) throws Exception {
		LOG.info("活动对象：" + key + " ==> " + p.object.id)
		super.activateObject(key, p);
	}

	override destroyObject(String key, PooledObject<ObjectPoolWrapper<T>> p) throws Exception {
		LOG.info("销毁对象:" + key + " ==> " + p.object.id)
		super.destroyObject(key, p);
	}

	override passivateObject(String key, PooledObject<ObjectPoolWrapper<T>> p) throws Exception {
		LOG.info("归还对象:" + key + " ==> " + p.object.id)
		super.passivateObject(key, p);
	}
}

@Accessors
class KeyedObjectPoolUtils<T> {
	@Accessors
	static class ObjectPoolWrapper<T> implements AutoCloseable {
		long id;
		T instance;
		KeyedObjectPool<String, ObjectPoolWrapper<T>> pool;

		new(KeyedObjectPool<String, ObjectPoolWrapper<T>> pool, T instance, long id) {
			this.pool = pool;
			this.instance = instance;
			this.id = id;
		}

		override close() throws Exception {
			this.pool.returnObject(instance.class.name, this)
		}
	}

	GenericKeyedObjectPool<String, ObjectPoolWrapper<T>> pool;

	new(GenericKeyedObjectPoolConfig<ObjectPoolWrapper<T>> config) {
		this(config, new KeyedObjectPoolFactory<T>());
	}

	new(GenericKeyedObjectPoolConfig<ObjectPoolWrapper<T>> config, KeyedObjectPoolFactory<T> factory) {
		this(config, factory, Thread.currentThread.contextClassLoader);
	}

	new(GenericKeyedObjectPoolConfig<ObjectPoolWrapper<T>> config, KeyedObjectPoolFactory<T> factory, ClassLoader loader) {
		this.pool = new GenericKeyedObjectPool<String, ObjectPoolWrapper<T>>(factory, config);
		factory.pool = this.pool;
		factory.loader = loader;
	}

	def getInstance(String key) {
		return this.pool.borrowObject(key);
	}

	def remove(String key, ObjectPoolWrapper<T> obj) {
		this.pool.invalidateObject(key, obj);
	}

	def close(String key, ObjectPoolWrapper<T> obj) {
		this.pool.returnObject(key, obj);
	}
}
