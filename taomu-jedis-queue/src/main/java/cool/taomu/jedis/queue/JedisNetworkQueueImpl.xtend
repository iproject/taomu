/**
 * Copyright (c) 2023 murenchao
 * taomu is licensed under Mulan PubL v2.
 * You can use this software according to the terms and conditions of the Mulan PubL v2.
 * You may obtain a copy of Mulan PubL v2 at:
 *       http://license.coscl.org.cn/MulanPubL-2.0
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PubL v2 for more details.
 */
package cool.taomu.jedis.queue

import com.google.auto.service.AutoService
import com.google.gson.Gson
import cool.taomu.core.TaomuRunnable
import cool.taomu.core.entity.TaskEntity
import cool.taomu.core.inter.Callback
import cool.taomu.core.inter.ILock
import cool.taomu.core.inter.ITaskQueue
import cool.taomu.util.TimerLoop
import cool.taomu.util.YamlUtils
import cool.taomu.crypto.Base64
import java.util.ServiceLoader
import org.slf4j.LoggerFactory
import redis.clients.jedis.HostAndPort
import redis.clients.jedis.Jedis

@AutoService(ITaskQueue)
class JedisNetworkQueueImpl implements ITaskQueue {
	val static LOG = LoggerFactory.getLogger(JedisNetworkQueueImpl);
	val static JedisConfigEntity config = YamlUtils.read("./config/redis.yml", JedisConfigEntity);
	var gson = new Gson();
	var TimerLoop timerLoop = new TimerLoop();

	override void init() {
		LOG.info("jedis队列配置:{}", new Gson().toJson(config));
	}

	override add(Runnable e) {
		if (e instanceof TaomuRunnable) {
			var task = e.task;
			var dataEntity = gson.toJson(task);
			var b64 = new Base64(dataEntity.bytes).encode();
			try(var jedis = new Jedis(new HostAndPort(config.hosts.get(0).host,config.hosts.get(0).port))) {
				LOG.info("入队:{}==>{}", task.main_class, task.queue)
				jedis.lpush(task.queue, new String(b64, "UTF-8"));
				return true;
			}
		}
		return false;
	}

	override poll() {
		try(var	ILock lock = ServiceLoader.load(ILock).get(0)) {
			lock.tryLock
			try(var jedis = new Jedis(new HostAndPort(config.hosts.get(0).host, config.hosts.get(0).port))) {
				if (jedis.exists(config.taskQueue)) {
					var result = jedis.lpop(config.taskQueue);
					if (!result.nullOrEmpty) {
						var b64 = new Base64(result.bytes).decode();
						var task = new Gson().fromJson(new String(b64, "UTF-8"), TaskEntity);
						LOG.info("获取到的main_class:{}, queue:{}", task.main_class, task.queue);
						return new TaomuRunnable(task);
					}
				} else if (jedis.exists(config.shareTaskQueue)) {
					var sresult = jedis.lpop(config.shareTaskQueue);
					if (!sresult.nullOrEmpty) {
						var b64 = new Base64(sresult.bytes).decode();
						var task = new Gson().fromJson(new String(b64, "UTF-8"), TaskEntity);
						LOG.info("获取到的main_class:{}, queue:{}", task.main_class, task.queue);
						return new TaomuRunnable(task);
					} else {
						LOG.info("无数据");
					}
				}
			} catch (Exception ex) {
				LOG.info("数据异常:", ex);
			}
		}
		return null;
	}

	override trigger(Callback<TaomuRunnable> call) {
		timerLoop.loop([
			try(val jedis2 = new Jedis(new HostAndPort(config.hosts.get(0).host, config.hosts.get(0).port))) {
				var result = jedis2.blpop(0, config.taskQueue,config.shareTaskQueue);
				if (!result.nullOrEmpty) {
					var b64 = new Base64(result.get(1).bytes).decode();
					var task = new Gson().fromJson(new String(b64, "UTF-8"), TaskEntity);
					LOG.info("获取到的main_class:{}, queue:{}", task.main_class, task.queue);
					call.execute(new TaomuRunnable(task));
				}
			}
		])
		return timerLoop;
	}
}
