/**
 * Copyright (c) 2023 murenchao
 * taomu is licensed under Mulan PubL v2.
 * You can use this software according to the terms and conditions of the Mulan PubL v2.
 * You may obtain a copy of Mulan PubL v2 at:
 *       http://license.coscl.org.cn/MulanPubL-2.0
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PubL v2 for more details.
 */
package cool.taomu.crypto

import org.apache.commons.codec.digest.DigestUtils

class Md5 extends AbsCrypto {
	new(byte[] src) {
		this.src = src;
	}

	new(ICrypto c) {
		this.c = c;
	}

	override decode() {
		throw new UnsupportedOperationException("TODO: auto-generated method stub")
	}

	override encode() {
		if (this.c !== null) {
			return DigestUtils.md5Hex(this.c.encode()).bytes;
		}
		return DigestUtils.md5Hex(this.src).bytes;
	}
}
