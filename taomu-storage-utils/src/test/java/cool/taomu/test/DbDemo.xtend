package cool.taomu.test

import cool.taomu.storage.JdbcUtils
import com.mchange.v2.c3p0.ComboPooledDataSource

class DbDemo {
	def static void main(String[] args) {
		var ComboPooledDataSource dataSource = new ComboPooledDataSource("test");
		try(var db = new JdbcUtils(dataSource)){
			var a = db.queryMap("select * from stock_data limit 2");
			println(a)
		}
	}
}
