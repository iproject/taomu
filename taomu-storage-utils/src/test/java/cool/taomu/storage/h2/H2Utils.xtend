/**
 * Copyright (c) 2023 murenchao
 * taomu is licensed under Mulan PubL v2.
 * You can use this software according to the terms and conditions of the Mulan PubL v2.
 * You may obtain a copy of Mulan PubL v2 at:
 *       http://license.coscl.org.cn/MulanPubL-2.0
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PubL v2 for more details.
 */
package  cool.taomu.storage.h2

import cool.taomu.storage.JdbcUtils
import java.sql.Connection
import java.sql.DriverManager
import java.sql.SQLException
import java.util.List
import org.eclipse.xtend.lib.annotations.Accessors

@Accessors
class H2Utils {
	static final String DB_DRIVER = "org.h2.Driver";
	static final String DB_CONNECTION = "jdbc:h2:mem:test;DB_CLOSE_DELAY=-1";
	static final String DB_USER = "sa";
	static final String DB_PASSWORD = "";

	def Connection getConnection() {
		var Connection connection = null;
		try {
			Class.forName(DB_DRIVER);
			connection = DriverManager.getConnection(DB_CONNECTION, DB_USER, DB_PASSWORD);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return connection;
	}

	def <T> List<T> select(String sql, Class<T> zlass, Object ... params) {
		try(var jdbc = new JdbcUtils(this.connection)){
			return jdbc.queryEntity(sql, zlass, params);
		}
	}

	def modify(String sql) {
		return this.modify(sql, null);
	}

	def modify(String sql, Object ... params) {
		try(var jdbc = new JdbcUtils(this.connection)){
			jdbc.update(sql,params);
		}
	}
}
