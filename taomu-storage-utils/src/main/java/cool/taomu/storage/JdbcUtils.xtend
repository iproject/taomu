/**
 * Copyright (c) 2023 murenchao
 * taomu is licensed under Mulan PubL v2.
 * You can use this software according to the terms and conditions of the Mulan PubL v2.
 * You may obtain a copy of Mulan PubL v2 at:
 *       http://license.coscl.org.cn/MulanPubL-2.0
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PubL v2 for more details.
 */
package  cool.taomu.storage

import java.sql.Connection
import java.sql.SQLException
import java.util.List
import java.util.Map
import javax.sql.DataSource
import org.apache.commons.dbutils.QueryRunner
import org.apache.commons.dbutils.handlers.BeanListHandler
import org.apache.commons.dbutils.handlers.MapListHandler
import org.slf4j.LoggerFactory

class JdbcUtils extends QueryRunner implements AutoCloseable {
	val static LOG = LoggerFactory.getLogger(JdbcUtils);
	Connection conn = null;

	new(Connection conn) {
		this.conn = conn;
	}

	new(DataSource ds) {
		this.conn = ds.connection;
	}

	static interface Callback {
		def void execute(JdbcUtils jdbc) throws SQLException;
	}

	def void transaction(Callback cl) {
		try {
			LOG.info("执行数据库事务")
			this.conn.autoCommit = false;
			cl.execute(this)
			this.conn.commit;
		} catch (SQLException ex) {
			LOG.info("Sql  exception:", ex);
			this.conn.rollback;
		}
	}

	override int update(String sql, Object ... args) {
		LOG.info("sql:{}", sql);
		return this.update(conn, sql, args);
	}

	def int[] batch(String sql, List<List<Object>> args) {
		LOG.info("sql:{}", sql);
		var Object[][] array = newArrayOfSize(args.size, 0);
		for (var int i = 0; i < array.size; i++) {
			array.set(i, args.get(i).toArray);
		}
		return this.batch(conn, sql, array);
	}

	def <T> List<T> queryEntity(String sql, Class<T> zlass, Object ... args) {
		LOG.info("sql:{}", sql);
		var List<T> results = null;
		if (zlass !== null && !zlass.equals(Map)) {
			results = this.query(conn, sql, new BeanListHandler<T>(zlass), args);
		}
		return results;
	}

	def queryMap(String sql, Object ... args) {
		LOG.info("sql:{}", sql);
		return this.query(conn, sql, new MapListHandler(), args);
	}

	override close() throws Exception {
		if (this.conn !== null) {
			LOG.info("关闭数据库连接")
			this.conn.close
		}
	}

}