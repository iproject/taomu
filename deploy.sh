#! /bin/bash

pass=mrc2018
remote_directory=/home/rcmu/workspace/taomu


function config(){
	dfile=./startup_app.sh
	sshpass -p $pass scp -o StrictHostKeyChecking=no $dfile rcmu@c200.taomu.cool:$remote_directory
	sshpass -p $pass scp -o StrictHostKeyChecking=no $dfile rcmu@c221.taomu.cool:$remote_directory
	sshpass -p $pass scp -o StrictHostKeyChecking=no $dfile rcmu@c201.taomu.cool:$remote_directory
	sshpass -p $pass scp -o StrictHostKeyChecking=no $dfile rcmu@c202.taomu.cool:$remote_directory
	sshpass -p $pass scp -o StrictHostKeyChecking=no $dfile rcmu@c223.taomu.cool:$remote_directory
	
	dfile=./taomu.yml
	sshpass -p $pass scp -o StrictHostKeyChecking=no $dfile rcmu@c200.taomu.cool:$remote_directory
	sshpass -p $pass scp -o StrictHostKeyChecking=no $dfile rcmu@c221.taomu.cool:$remote_directory
	sshpass -p $pass scp -o StrictHostKeyChecking=no $dfile rcmu@c201.taomu.cool:$remote_directory
	sshpass -p $pass scp -o StrictHostKeyChecking=no $dfile rcmu@c202.taomu.cool:$remote_directory
	sshpass -p $pass scp -o StrictHostKeyChecking=no $dfile rcmu@c223.taomu.cool:$remote_directory
	dfile=./broker.yml
	sshpass -p $pass scp -o StrictHostKeyChecking=no $dfile rcmu@c223.taomu.cool:$remote_directory
	dfile=./startup_mqtt.sh
	sshpass -p $pass scp -o StrictHostKeyChecking=no $dfile rcmu@c223.taomu.cool:$remote_directory
}


function publish(){
gradle clean build -x test && gradle publish  && gradle jC
cd ./taomu-main/build/libs
#ls -l
#cp taomu-app-*.jar deps
ls -l
tar zcvf taomu.tar.gz deps
ls -l
}

function deploy(){
cd ~/nworkspace/taomu && gradle clean build -x test && gradle jC && cd taomu-main/build/libs && tar zcvf taomu.tar.gz deps
sshpass -p mrc2018 scp taomu.tar.gz rcmu@192.168.1.220:/home/rcmu/workspace &&  sshpass -p mrc2018 scp taomu.tar.gz rcmu@192.168.1.221:/home/rcmu/workspace  &&  sshpass -p mrc2018 scp taomu.tar.gz rcmu@192.168.1.223:/home/rcmu/workspace &&  sshpass -p mrc2018 scp taomu.tar.gz rcmu@192.168.1.224:/home/rcmu/workspace &&  sshpass -p mrc2018 scp taomu.tar.gz rcmu@192.168.1.225:/home/rcmu/workspace 
#&&  sshpass -p mrc2018 scp taomu.tar.gz rcmu@192.168.1.222:/home/rcmu/workspace
#&&  sshpass -p mrc2018 scp taomu.tar.gz rcmu@192.168.1.226:/home/rcmu/workspace
#&&  sshpass -p mrc2018 scp taomu.tar.gz rcmu@192.168.1.227:/home/rcmu/workspace
}


case "$1" in
    deploy)
        deploy
        ;;
    config)
        config
        ;;
    publish)
        publish
        ;;
    *)
        echo $"Usage: $0 {deploy|config|publish}"
        exit 1
esac
