/**
 * Copyright (c) 2023 murenchao
 * taomu is licensed under Mulan PubL v2.
 * You can use this software according to the terms and conditions of the Mulan PubL v2.
 * You may obtain a copy of Mulan PubL v2 at:
 *       http://license.coscl.org.cn/MulanPubL-2.0
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PubL v2 for more details.
 */
package cool.taomu.mqtt.broker.impl

import cool.taomu.mqtt.broker.utils.inter.IObserver

class RetainObservable extends AObservable<IObserver>{
	
	val static instance = new RetainObservable();	
	private new(){}
	
	def static getInstance(){
		return instance;
	}
	
	override publish(Object arg, Object... args) {
		this.providers.values.forEach[
			exec.submit([
				it.publish(this,arg);
			])
		]	
	}
	
}
