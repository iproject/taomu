/**
 * Copyright (c) 2023 murenchao
 * taomu is licensed under Mulan PubL v2.
 * You can use this software according to the terms and conditions of the Mulan PubL v2.
 * You may obtain a copy of Mulan PubL v2 at:
 *       http://license.coscl.org.cn/MulanPubL-2.0
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PubL v2 for more details.
 */
package cool.taomu.mqtt.broker.factory

import io.netty.channel.ChannelHandlerContext
import io.netty.handler.codec.mqtt.MqttFixedHeader
import io.netty.handler.codec.mqtt.MqttMessage
import io.netty.handler.codec.mqtt.MqttMessageType
import io.netty.handler.codec.mqtt.MqttQoS
import org.slf4j.LoggerFactory

import static extension cool.taomu.mqtt.broker.utils.MqttUtils.*

class PingEqRequest implements IProcess {
	val static LOG = LoggerFactory.getLogger(PingEqRequest);

	override request(ChannelHandlerContext ctx, MqttMessage mqttMessage) {
		var clientId = ctx.channel.clientId;
		LOG.debug("执行了MQTT PingEq 命令 : " + clientId);
		var mfh = new MqttFixedHeader(MqttMessageType.PINGRESP, false, MqttQoS.AT_MOST_ONCE, false, 0);
		var mm = new MqttMessage(mfh);
		ctx.writeAndFlush(mm);
	}
}
