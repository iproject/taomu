/**
 * Copyright (c) 2023 murenchao
 * taomu is licensed under Mulan PubL v2.
 * You can use this software according to the terms and conditions of the Mulan PubL v2.
 * You may obtain a copy of Mulan PubL v2 at:
 *       http://license.coscl.org.cn/MulanPubL-2.0
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PubL v2 for more details.
 */
package cool.taomu.mqtt.broker.factory

import io.netty.handler.codec.mqtt.MqttMessageType

class ProcessFactory {
	def static IProcess instance(MqttMessageType typename) {
		switch (typename) {
			case CONNECT: {
				return new ConnectRequest();
			}
			case PUBLISH: {
				return new PublishRequest();
			}
			case DISCONNECT: {
				return new DisconnectRequest();
			}
			case PINGREQ: {
				return new PingEqRequest();
			}
			case PUBACK: {
				return new PubAckRequest();
			}
			case PUBCOMP: {
				return new PubCompRequest();
			}
			case PUBREC: {
				return new PubRecRequest();
			}
			case PUBREL: {
				return new PubRelRequest();
			}
			case SUBSCRIBE: {
				return new SubscribeRequest();
			}
			case UNSUBSCRIBE: {
				return new UnSubscribeRequest();
			}
			default: {
			}
		}
	}
}
