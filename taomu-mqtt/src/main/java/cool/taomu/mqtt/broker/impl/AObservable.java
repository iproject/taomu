/**
 * Copyright (c) 2023 murenchao
 * taomu is licensed under Mulan PubL v2.
 * You can use this software according to the terms and conditions of the Mulan PubL v2.
 * You may obtain a copy of Mulan PubL v2 at:
 *       http://license.coscl.org.cn/MulanPubL-2.0
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PubL v2 for more details.
 */
package cool.taomu.mqtt.broker.impl;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.google.common.collect.ArrayListMultimap;

import cool.taomu.mqtt.broker.utils.inter.IObservable;

public abstract class AObservable<T extends Object> implements IObservable<T> {
	protected final ArrayListMultimap<Object, T> providers = ArrayListMultimap.<Object, T>create();
	//clientId topic+qos message

	protected final static ExecutorService exec = Executors.newCachedThreadPool();

	static {
		Runtime.getRuntime().addShutdownHook(new Thread() {
			public void run() {
				exec.shutdown();
			}
		});
	}

	public void register(final Object arg, final T observer) {
		this.providers.put(arg, observer);
	}

	public void unregister(final Object arg) {
		this.providers.removeAll(arg);
	}
}
