/**
 * Copyright (c) 2023 murenchao
 * taomu is licensed under Mulan PubL v2.
 * You can use this software according to the terms and conditions of the Mulan PubL v2.
 * You may obtain a copy of Mulan PubL v2 at:
 *       http://license.coscl.org.cn/MulanPubL-2.0
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PubL v2 for more details.
 */
package cool.taomu.mqtt.broker.factory

import cool.taomu.mqtt.broker.impl.PublishObservable
import cool.taomu.mqtt.broker.utils.MqttUtils
import cool.taomu.mqtt.broker.utils.inter.IObservable
import cool.taomu.mqtt.broker.utils.inter.IObserver
import io.netty.channel.ChannelHandlerContext
import io.netty.handler.codec.mqtt.MqttFixedHeader
import io.netty.handler.codec.mqtt.MqttMessage
import io.netty.handler.codec.mqtt.MqttMessageIdVariableHeader
import io.netty.handler.codec.mqtt.MqttMessageType
import io.netty.handler.codec.mqtt.MqttQoS
import io.netty.handler.codec.mqtt.MqttUnsubAckMessage
import io.netty.handler.codec.mqtt.MqttUnsubscribeMessage
import org.slf4j.LoggerFactory

class UnSubscribeRequest implements IProcess {
	val static LOG = LoggerFactory.getLogger(UnSubscribeRequest);
	val static IObservable<IObserver> observable = PublishObservable.instance;

	override request(ChannelHandlerContext ctx, MqttMessage mqttMessage) {
		var message = mqttMessage as MqttUnsubscribeMessage;
		var messageId = message.variableHeader.messageId;
		var topics = message.payload.topics;
		val clientId = MqttUtils.getClientId(ctx.channel);
		topics.forEach [
			LOG.info("unsubscribe clientId {} topics {}", clientId, it);
			observable.unregister(#[clientId, it].join("#"));
		]
		var header = new MqttFixedHeader(MqttMessageType.UNSUBACK, false, MqttQoS.AT_MOST_ONCE, false, 0);
		var varHeader = MqttMessageIdVariableHeader.from(messageId);
		ctx.writeAndFlush(new MqttUnsubAckMessage(header, varHeader));
	}
}
