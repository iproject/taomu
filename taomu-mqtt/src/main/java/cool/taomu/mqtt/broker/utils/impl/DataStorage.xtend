/**
 * Copyright (c) 2023 murenchao
 * taomu is licensed under Mulan PubL v2.
 * You can use this software according to the terms and conditions of the Mulan PubL v2.
 * You may obtain a copy of Mulan PubL v2 at:
 *       http://license.coscl.org.cn/MulanPubL-2.0
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PubL v2 for more details.
 */
package cool.taomu.mqtt.broker.utils.impl

import cool.taomu.storage.inter.IStorage
import java.io.Serializable

class DataStorage implements IStorage {
	val IStorage storage = new GuavaCache;

	override put(String identifier, String key, Serializable value) {
		storage.put(identifier, key, value);
	}

	override get(String identifier, String key) {
		return storage.get(identifier, key);
	}

	override remove(String identifier, String key) {
		storage.remove(identifier, key);
	}

	override clear(String identifier) {
		throw new UnsupportedOperationException("TODO: auto-generated method stub")
	}
}
