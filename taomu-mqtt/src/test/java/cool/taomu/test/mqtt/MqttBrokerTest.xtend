package cool.taomu.test.mqtt

import cool.taomu.mqtt.broker.MqttBrokerConfigure
import cool.taomu.mqtt.broker.MQTTBroker

class MqttBrokerTest {
	def static void main(String[] args) {
		var config = new MqttBrokerConfigure();
		var broker = new MQTTBroker(config.read());
		broker.startTcpServer();
	}
}