#! /bin/bash

RUN_NAME="broker"
MAIN_CLASS="cool.taomu.mqtt.broker.BrokerMain"
JAVA_OPTS=""
CLASSPATH=.$CLASSPATH
DIR=./deps/*.jar
for i in $DIR ;do
    CLASSPATH=$CLASSPATH:$i
done

export JAVA_HOME=$JAVA_HOME
#export CLASSPATH=$CLASSPATH

function start(){
    echo $JAVA_HOME
    echo $CLASSPATH
    echo $MAIN_CLASS
    echo "$RUN_NAME trying to start ....."
    nohup java -cp $CLASSPATH $MAIN_CLASS $2 $3 >> ./test.log 2>&1 &
    echo "$RUN_NAME started success."
}

function stop(){
    echo "Stopping $RUN_NAME ..."
    kill -9 `ps -ef|grep $RUN_NAME|grep -v grep|grep -v stop|awk '{print $2}'`
}

case "$1" in
    start)
        start -c broker.yml
        ;;
    stop)
        stop
        ;;
    restart)
        stop
        start -c broker.yml
        ;;
    *)
        echo $"Usage: $0 {start|stop|restart}"
        exit 1
esac
    
