package cool.taomu.tomcat.embed

import com.google.inject.Injector
import cool.taomu.service.ITaomuService
import cool.taomu.servlet.TaomuServlet
import java.io.File
import org.apache.catalina.LifecycleException
import org.apache.catalina.startup.Tomcat
import org.slf4j.LoggerFactory

class TomcatServer implements ITaomuService {
	val static LOG = LoggerFactory.getLogger(TomcatServer);

	override start(Injector injector) {
		var projectPatha = System.getProperty("user.dir")
		var webAppPath = #[projectPatha, "WebRoot"].join(File.separator)
		var catalinaHome = #[projectPatha, "Embedded", "Tomcat"].join(File.separator)
		val tomcat = new Tomcat();
		Runtime.getRuntime().addShutdownHook(
			new Thread([
				try {
					tomcat.destroy();
				} catch (LifecycleException e) {
					e.printStackTrace();
				}
			])
		);
		tomcat.baseDir = catalinaHome
		tomcat.port = 8080
		tomcat.connector;
		var ctx = tomcat.addContext("", webAppPath);
///		var ctx = tomcat.addWebapp("/taomu", webAppPath);
		Tomcat.addServlet(ctx, "base", new TaomuServlet(injector));
		ctx.addServletMappingDecoded("/", "base");
		tomcat.start();
		tomcat.server.await();
	}

	override stop() {
	}
}
