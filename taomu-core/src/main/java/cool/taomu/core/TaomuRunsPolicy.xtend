/**
 * Copyright (c) 2023 murenchao
 * taomu is licensed under Mulan PubL v2.
 * You can use this software according to the terms and conditions of the Mulan PubL v2.
 * You may obtain a copy of Mulan PubL v2 at:
 *       http://license.coscl.org.cn/MulanPubL-2.0
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PubL v2 for more details.
 */
package cool.taomu.core

import cool.taomu.core.inter.ITaskQueue
import java.util.concurrent.ThreadPoolExecutor
import java.util.concurrent.ThreadPoolExecutor.CallerRunsPolicy
import org.slf4j.LoggerFactory
import java.util.List

class TaomuRunsPolicy extends CallerRunsPolicy {
	val static LOG = LoggerFactory.getLogger(TaomuRunsPolicy);
	val List<ITaskQueue> queue;

	new(List<ITaskQueue> queue) {
		this.queue = queue;
	}

	def dispatch rejectedExecution(TaomuRunnable r, ThreadPoolExecutor executor) {
		LOG.info("TaomuTask当前被拒绝任务为:{}",r.task.main_class);
		try {
			queue.forEach[it.add(r)]
		} catch (Exception e) {
			LOG.info("网络异常：", e);
			super.rejectedExecution(r, executor);
		}
	}

	def dispatch rejectedExecution(Runnable r, ThreadPoolExecutor executor) {
		LOG.info("当前被拒绝任务为:{}", r.toString());
		super.rejectedExecution(r, executor);
	}

}
