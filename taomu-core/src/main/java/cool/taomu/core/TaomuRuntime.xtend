/**
 * Copyright (c) 2023 murenchao
 * taomu is licensed under Mulan PubL v2.
 * You can use this software according to the terms and conditions of the Mulan PubL v2.
 * You may obtain a copy of Mulan PubL v2 at:
 *       http://license.coscl.org.cn/MulanPubL-2.0
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PubL v2 for more details.
 */
package cool.taomu.core

import cool.taomu.core.entity.TaskEntity
import cool.taomu.core.entity.TimerEntity.Operation
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.ScheduledFuture
import org.slf4j.LoggerFactory

class TaomuRuntime {
	val static LOG = LoggerFactory.getLogger(TaomuTask);
	val static timerTasks = new ConcurrentHashMap<String, ScheduledFuture<?>>();
	var static TaomuThreadPoolFactory ttpf = TaomuThreadPoolFactory.instance;
	val static instance = new TaomuRuntime();

	private new() {
	}

	def static getInstance() {
		return instance;
	}

	def void execute(TaomuRunnable task) {
		synchronized (task) {
			if (task.task.timer !== null) {
				this.threadPoolTimer(task.task,task);
			} else {
				this.threadPool(task);
			}
		}
	}

	private def void task(TaomuRunnable tr) {
		LOG.info("调用线程池执行任务:{}", tr.task.main_class);
		ttpf.getThreadPool.execute(tr);
	}

	def  threadPoolTimer(TaskEntity timerTask,TaomuRunnable tr) {
		LOG.info("执行定时任务:{}", timerTask.uuid);
		var task = timerTask.timer;
		if (!timerTasks.containsKey(timerTask.uuid) && task.operation === Operation.Start) {
			var scheduled = ttpf.getscheduledThreadPool;
			LOG.info("启动定时任务:{}", timerTask.uuid);
			var runnable = new Runnable() {
				override run() {
					TaomuRuntime.this.task(tr)
				}

			}
			switch (task.type) {
				case Schedule: {
					timerTasks.put(timerTask.uuid, scheduled.schedule(runnable, task.delay, task.timeUit));
				}
				case Schedule_at_fixed_rate: {
					timerTasks.put(timerTask.uuid,
						scheduled.scheduleAtFixedRate(runnable, task.initialDelay, task.delay, task.timeUit));
				}
				case Schedule_with_fixed_delay: {
					timerTasks.put(timerTask.uuid,
						scheduled.scheduleWithFixedDelay(runnable, task.initialDelay, task.delay, task.timeUit));
				}
			}
		} else if (timerTasks.containsKey(timerTask) && task.operation === Operation.Stop) {
			LOG.info("关闭定时任务:{}", timerTask.uuid);
			timerTasks.get(timerTask.uuid).cancel(true);
		}
	}

	def  threadPool(TaomuRunnable tr) {
		TaomuRuntime.this.task(tr);
	}

}
