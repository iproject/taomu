/**
 * Copyright (c) 2023 murenchao
 * taomu is licensed under Mulan PubL v2.
 * You can use this software according to the terms and conditions of the Mulan PubL v2.
 * You may obtain a copy of Mulan PubL v2 at:
 *       http://license.coscl.org.cn/MulanPubL-2.0
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PubL v2 for more details.
 */
package cool.taomu.core;

import java.util.Iterator;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.eclipse.xtext.xbase.lib.IteratorExtensions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;

import cool.taomu.core.inter.INote;
import cool.taomu.core.inter.ITaskQueue;
import cool.taomu.util.RuntimeInfo;
import cool.taomu.util.TaomuServiceLoader;

public class TaomuThreadPoolFactory {
	private static final Logger LOG = LoggerFactory.getLogger(TaomuThreadPoolFactory.class);

	private static final int coreSize = Runtime.getRuntime().availableProcessors();

	private static ThreadPoolExecutor threadPool = null;

	private static ScheduledThreadPoolExecutor scheduledThreadPool = null;
	private static final TaomuThreadPoolFactory instance = new TaomuThreadPoolFactory();
	private TaomuThreadPoolFactory() {}

	static {
		Timer timer = new Timer("JvmInfoTimer");
		timer.schedule(new TimerTask() {
			@Override
			public void run() {
				Iterator<Class<INote>> sl = new TaomuServiceLoader<INote>().loader(INote.class).iterator();
				if (!IteratorExtensions.isNullOrEmpty(sl)) {
					LOG.info("开始threadPool获取jvm运行信息");
					RuntimeInfo info = new RuntimeInfo();
					String jsonStr = new Gson().toJson(info);
					while (sl.hasNext()) 
					{
						try {
							INote note = (INote) sl.next().newInstance();
							note.write(jsonStr);
						} catch (InstantiationException | IllegalAccessException e) {
							e.printStackTrace();
						}
					}
				}else {
					LOG.info("关闭定时器");
					timer.cancel();
				}
			}
		}, 0, 10 * 1000);
	}

	public ThreadPoolExecutor getThreadPool() {
		return TaomuThreadPoolFactory.threadPool;
	}

	public ScheduledThreadPoolExecutor getscheduledThreadPool() {
		return TaomuThreadPoolFactory.scheduledThreadPool;
	}
	
	public static TaomuThreadPoolFactory getInstance() {
		return instance;
	}

	public static void init(BlockingQueue<Runnable>  workQueue,List<ITaskQueue> inq) {
		LOG.info("初始化线程池");
		threadPool = new ThreadPoolExecutor(TaomuThreadPoolFactory.coreSize, TaomuThreadPoolFactory.coreSize, 60,
				TimeUnit.SECONDS, workQueue, new TaomuRunsPolicy(inq));
		scheduledThreadPool = new ScheduledThreadPoolExecutor(TaomuThreadPoolFactory.coreSize,
				new TaomuRunsPolicy(inq));
		Runtime.getRuntime().addShutdownHook(new Thread() {
			public void run() {
				TaomuThreadPoolFactory.LOG.info("关闭线程池");
				TaomuThreadPoolFactory.threadPool.shutdown();
				TaomuThreadPoolFactory.scheduledThreadPool.shutdown();
			}
		});
	}
}
