/**
 * Copyright (c) 2023 murenchao
 * taomu is licensed under Mulan PubL v2.
 * You can use this software according to the terms and conditions of the Mulan PubL v2.
 * You may obtain a copy of Mulan PubL v2 at:
 *       http://license.coscl.org.cn/MulanPubL-2.0
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PubL v2 for more details.
 */
package cool.taomu.core.lang

import cool.taomu.core.entity.TaskEntity.ScriptType
import java.util.concurrent.ConcurrentSkipListMap
import org.slf4j.LoggerFactory
import cool.taomu.util.TaomuServiceLoader

class WrapperClassLoaderFactory {
	static val LOG = LoggerFactory.getLogger(WrapperClassLoaderFactory);
	val static ConcurrentSkipListMap<ScriptType,Class<AbsWrapperClassLoader>> cache= new ConcurrentSkipListMap();
	def static Class<AbsWrapperClassLoader> get(ScriptType type){
		if(cache.empty){
			var wrappers = new TaomuServiceLoader<AbsWrapperClassLoader>().loader(AbsWrapperClassLoader).iterator;
			wrappers.forEach[
				var ann = it.getAnnotation(WrapperClassLoader);
				LOG.info("加载类加载器:{}",type);
				cache.put(ann.value,it);
			]
		}
		if(cache.containsKey(type)){
			return cache.get(type);
		}else{
			LOG.info("没有{}类加载器",type);
			return null;
		}
	}
}