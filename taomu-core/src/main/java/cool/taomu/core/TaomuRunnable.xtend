/**
 * Copyright (c) 2023 murenchao
 * taomu is licensed under Mulan PubL v2.
 * You can use this software according to the terms and conditions of the Mulan PubL v2.
 * You may obtain a copy of Mulan PubL v2 at:
 *       http://license.coscl.org.cn/MulanPubL-2.0
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PubL v2 for more details.
 */
package cool.taomu.core

import cool.taomu.core.entity.TaskEntity
import cool.taomu.core.entity.TaskEntity.DeplyType
import cool.taomu.core.inter.ITaomuEntry
import cool.taomu.core.lang.MultiClassLoader
import cool.taomu.core.lang.WrapperClassLoaderFactory
import org.apache.commons.lang3.Validate
import org.eclipse.xtend.lib.annotations.Accessors
import org.slf4j.LoggerFactory
import java.io.Serializable

@Accessors
class TaomuRunnable implements Runnable,Serializable {
	val static LOG = LoggerFactory.getLogger(TaomuRunnable);
	val static mcl = MultiClassLoader.instnace;
	TaskEntity task;

	new() {
	}

	new(TaskEntity task) {
		LOG.info("实例任务:{}", task.main_class);
		this.task = task;
	}

	override run() {
		synchronized (TaskEntity) {
			var script = Validate.notBlank(task.script, "script 不能为空");
			try {
				// var flag = new String(new Md5(script.bytes).encode(), "UTF-8");
				LOG.info("开始执行任务:{}", task.main_class);
				var zlass = mcl.loadClass(task.main_class);
				//TODO 脚本编译策略需要修改 
				if ((zlass === null || task.deplyType == DeplyType.Redeployment) && !script.nullOrEmpty) {
					LOG.info("加载任务脚本")
					var wrapper = WrapperClassLoaderFactory.get(task.scriptType);
					mcl.add(task.scriptType, task.uuid, script, wrapper.newInstance);
					zlass = mcl.loadClass(task.main_class);
				}
				var instance = zlass.newInstance as ITaomuEntry;
				instance.entry(script, task.payload);
			} catch (Exception ex) {
				LOG.info("执行异常:", ex);
				try {
					if (!script.nullOrEmpty) {
						LOG.info("重新加载任务脚本")
						var wrapper = WrapperClassLoaderFactory.get(task.scriptType);
						mcl.add(task.scriptType, task.uuid, script, wrapper.newInstance);
						var zlass = mcl.loadClass(task.main_class);
						var instance = zlass.newInstance as ITaomuEntry;
						instance.entry(script, task.payload);
					}
				} catch (Exception ex1) {
					LOG.info("执行异常2:", ex);
				}
			}
		}
	}

}
