/**
 * Copyright (c) 2023 murenchao
 * taomu is licensed under Mulan PubL v2.
 * You can use this software according to the terms and conditions of the Mulan PubL v2.
 * You may obtain a copy of Mulan PubL v2 at:
 *       http://license.coscl.org.cn/MulanPubL-2.0
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PubL v2 for more details.
 */
package cool.taomu.util

import cool.taomu.core.inter.Callback
import java.awt.event.ActionEvent
import java.awt.event.ActionListener
import java.util.HashSet
import javax.swing.Timer
import org.eclipse.xtend.lib.annotations.Accessors

class TimerLoop {
	var timerPool = new HashSet<Timer>();
	int delay = 1;
	
	new(){}
	new(int delay){
		this.delay = delay;
	}

	@Accessors
	static class TimerExecute implements ActionListener {
		var Timer timer;
		var Callback<Void> call;

		new(Callback<Void> call) {
			this.call = call;
		}

		override actionPerformed(ActionEvent e) {
			if (timer.isRunning) {
				timer.stop;
				if (call !== null) {
					this.call.execute(null);
				}
				timer.start;
			}
		}
	}

	def loop(Callback<Void> call) {
		var te = new TimerExecute(call);
		te.timer = new Timer(this.delay , te);
		te.timer.setCoalesce(false);
		timerPool.add(te.timer);
	}

	def stop() {
		timerPool.forEach[it.stop();]
	}

	def start() {
		timerPool.forEach[it.start();]
	}
}
