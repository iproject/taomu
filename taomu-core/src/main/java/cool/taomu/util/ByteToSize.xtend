/**
 * Copyright (c) 2023 murenchao
 * taomu is licensed under Mulan PubL v2.
 * You can use this software according to the terms and conditions of the Mulan PubL v2.
 * You may obtain a copy of Mulan PubL v2 at:
 *       http://license.coscl.org.cn/MulanPubL-2.0
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PubL v2 for more details.
 */
package cool.taomu.util

import java.text.DecimalFormat
import java.math.BigDecimal

class ByteToSize {
	def static void main(String[] args) {
		println(sizeDescription("11258999068426"));
	}

	def static sizeDescription(String size) {
		var bsize = new BigDecimal(size);
		var format = new DecimalFormat("###.0");
		if (bsize.compareTo(new BigDecimal("1125899906842624")) > -1) {
			var a = new BigDecimal("1125899906842624");
			var result = bsize.divide(a).longValue;
			return #[format.format(result), "PB"].join()
		} else if (bsize.compareTo(new BigDecimal("1099511627776")) > -1) {
			var a = new BigDecimal("1099511627776");
			var result = bsize.divide(a).longValue;
			return #[format.format(result), "TB"].join()
		} else if (bsize.longValue >= 1024 * 1024 * 1024) {
			var result = (bsize.longValue / (1024.0 * 1024.0 * 1024.0));
			return #[format.format(result), "GB"].join()
		} else if (bsize.longValue >= 1024 * 1024) {
			var result = (bsize.longValue / (1024.0 * 1024.0));
			return #[format.format(result), "MB"].join()
		} else if (bsize.longValue >= 1024) {
			var result = (bsize.longValue / 1024.0);
			return #[format.format(result), "KB"].join()
		} else if (bsize.longValue < 1024) {
			if (bsize.longValue <= 0) {
				return "0B";
			} else {
				return #[size, "B"].join();
			}
		}
	}

}
