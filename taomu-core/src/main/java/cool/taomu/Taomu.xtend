/**
 * Copyright (c) 2023 murenchao
 * taomu is licensed under Mulan PubL v2.
 * You can use this software according to the terms and conditions of the Mulan PubL v2.
 * You may obtain a copy of Mulan PubL v2 at:
 *       http://license.coscl.org.cn/MulanPubL-2.0
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PubL v2 for more details.
 */
package cool.taomu

import cool.taomu.core.TaomuTask
import cool.taomu.core.inter.ITaomuService
import cool.taomu.util.RuntimeInfo
import cool.taomu.util.TaomuServiceLoader
import java.io.File
import org.apache.commons.io.FileUtils
import org.slf4j.LoggerFactory

class Taomu {
	val static LOG = LoggerFactory.getLogger(Taomu);

	def static void main(String[] args) {
		var ri = new RuntimeInfo();
		val File pid = new File("./taomu.pid");
		FileUtils.writeStringToFile(pid, ri.pid, "UTF-8");
		var TaomuServiceLoader<ITaomuService> itrs = new TaomuServiceLoader().loader(ITaomuService);
		if (!itrs.nullOrEmpty) {
			itrs.iterator.forEach [
				var itr = it.newInstance as ITaomuService
				itr.service(TaomuTask.instance);
			]
		}
		LOG.info("启动成功")
		Runtime.runtime.addShutdownHook(new Thread() {
			override run() {
				pid.delete;
			}
		})
	}
}
