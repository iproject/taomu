package cool.taomu.thrift.entity

import org.eclipse.xtend.lib.annotations.Accessors

@Accessors
class ThriftConfig {
	String host;
	int port;
	int maxThreads;
	int minThreads;
	Integer selector;
}
