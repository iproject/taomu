package cool.taomu.servlet.entity

import com.google.gson.Gson
import jakarta.servlet.http.HttpServletRequest
import jakarta.servlet.http.HttpServletResponse
import java.io.Serializable
import java.io.Writer

abstract class AbsEventEntity implements Serializable {
	protected HttpServletRequest request;
	protected HttpServletResponse response;

	def void init(HttpServletRequest request, HttpServletResponse response) {
		this.request = request;
		this.response = response;
	}

	def void Return(Object ret) {
		Return(ret,"UTF-8");
	}
	def void Return(Object ret,String charset) {
		response.setCharacterEncoding(charset);
		try (var Writer writer = response.getWriter()) {
			writer.write(new Gson().toJson(ret));
			writer.flush();
		}
	}
}
