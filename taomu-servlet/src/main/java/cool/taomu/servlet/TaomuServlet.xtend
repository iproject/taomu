package cool.taomu.servlet

import com.google.common.eventbus.AsyncEventBus
import com.google.common.eventbus.EventBus
import com.google.gson.Gson
import com.google.inject.Injector
import com.google.inject.name.Names
import cool.taomu.guice.Key
import cool.taomu.servlet.entity.AbsEventEntity
import jakarta.servlet.ServletException
import jakarta.servlet.http.HttpServlet
import jakarta.servlet.http.HttpServletRequest
import jakarta.servlet.http.HttpServletResponse
import java.io.IOException
import java.util.concurrent.Executors
import org.apache.commons.io.IOUtils
import org.slf4j.LoggerFactory

class TaomuServlet extends HttpServlet {
	val static LOG = LoggerFactory.getLogger(TaomuServlet);
	var aBus = new AsyncEventBus(Executors.newCachedThreadPool);
	var bus = new EventBus();
	Injector injector;

	new(Injector injector) {
		this.injector = injector;
	}

	override void service(HttpServletRequest request,
		HttpServletResponse response) throws ServletException, IOException {
		var name = request.requestURI
		LOG.info("请求链接:{}", name);
		var ctrl = injector.getInstance(Key.get(Names.named(name)));
		if (ctrl.class.getAnnotation(Async) !== null) {
			LOG.info("注册到异步");
			aBus.register(ctrl);
			this.bus = aBus;
		} else {
			LOG.info("注册到同步");
			this.bus.register(ctrl);
		}
		var pp = request.parameterNames;
		while(pp.hasMoreElements){
			println(pp.nextElement);
		}
		
		println(request.parameterMap.mapValues[it.immutableCopy])
		val zlassName = name.substring(name.lastIndexOf("/") + 1)
		var m = ctrl.class.methods.findFirst [
			var topic = it.getAnnotation(Topic)
			topic !== null && topic.value.equals(zlassName)
		]
		if (m !== null) {
			var typeName = m.parameters.get(0).parameterizedType.typeName
			if (request.method.equals("POST")) {
				var jsonStr = IOUtils.toString(request.inputStream, "UTF-8");
				var zlass = Thread.currentThread.contextClassLoader.loadClass(typeName);
				if (zlass === null) {
					zlass = ClassLoader.systemClassLoader.loadClass(typeName)
				}
				var a = new Gson().fromJson(jsonStr, zlass);
				if (a !== null) {
					(a as AbsEventEntity).init(request, response)
					this.bus.post(a);
				}
			}
		}
	}
}
