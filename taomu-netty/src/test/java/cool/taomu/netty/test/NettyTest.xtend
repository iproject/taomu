package cool.taomu.netty.test

import com.google.inject.Provider
import cool.taomu.guice.TaomuGuice
import cool.taomu.guice.ann.Binder
import cool.taomu.guice.ann.Binders
import cool.taomu.netty.NettyClient
import cool.taomu.netty.NettyServer
import cool.taomu.netty.inter.IClientConfigure
import cool.taomu.netty.inter.IServerConfigure
import cool.taomu.netty.test.demo.DemoClientConfigure
import cool.taomu.netty.test.demo.DemoServerConfigure
import javax.inject.Inject

class NettyServerService implements Provider<NettyServer> {
	@Inject
	IServerConfigure isc;

	override get() {
		var ns = new NettyServer("127.0.0.1", 30001);
		ns.start(isc);
		return ns
	}
}

class NettyClientService implements Provider<NettyClient> {
	@Inject
	IClientConfigure icc

	override get() {
		var nc = new NettyClient("127.0.0.1", 30001);
		nc.client(icc);
		return nc;
	}
}

@Binders(#[
	@Binder(bind=IServerConfigure, to=DemoServerConfigure),
	@Binder(bind=IClientConfigure, to=DemoClientConfigure),
	@Binder(bind=NettyServer, to=NettyServerService, provider=true),
	@Binder(bind=NettyClient, to=NettyClientService, provider=true)
])
class NettyTest {
	def static void main(String[] args) {
		var tg = TaomuGuice.entry(NettyTest)
		tg.getInstance(NettyServer);
		tg.getInstance(NettyClient);
	}
}
