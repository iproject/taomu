package cool.taomu.netty.pipeline

import cool.taomu.netty.inter.IServerConfigure
import io.netty.bootstrap.ServerBootstrap
import io.netty.channel.ChannelHandler
import io.netty.channel.ChannelOption
import io.netty.channel.ChannelPipeline
import io.netty.channel.socket.SocketChannel
import io.netty.handler.codec.mqtt.MqttDecoder
import io.netty.handler.codec.mqtt.MqttEncoder
import io.netty.handler.timeout.IdleStateHandler

class MqttConfigure implements IServerConfigure {
	ChannelHandler handler

	new(ChannelHandler handler) {
		this.handler = handler;
	}

	override bootstrap(ServerBootstrap channel) {
		channel.option(ChannelOption.SO_BACKLOG, 1024).childOption(ChannelOption.TCP_NODELAY, false).option(
			ChannelOption.SO_REUSEADDR, true).childOption(ChannelOption.SO_KEEPALIVE, false);
	}

	override pipeline(ChannelPipeline pipeline,SocketChannel ch) {
		pipeline.addLast("idleStateHandler", new IdleStateHandler(60, 0, 0));
		pipeline.addLast("mqttEncoder", MqttEncoder.INSTANCE);
		pipeline.addLast("mqttDecoder", new MqttDecoder(Integer.MAX_VALUE));
		pipeline.addLast("nettyMqttHandler", handler);
	}

}
