package cool.taomu.netty.inter

import io.netty.bootstrap.ServerBootstrap
import io.netty.channel.ChannelPipeline
import io.netty.channel.socket.SocketChannel

interface IServerConfigure {
	def void bootstrap(ServerBootstrap bootstrap);
	def void pipeline(ChannelPipeline pipeline,SocketChannel ch)	
}
