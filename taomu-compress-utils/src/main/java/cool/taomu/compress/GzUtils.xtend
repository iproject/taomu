/**
 * Copyright (c) 2023 murenchao
 * taomu is licensed under Mulan PubL v2.
 * You can use this software according to the terms and conditions of the Mulan PubL v2.
 * You may obtain a copy of Mulan PubL v2 at:
 *       http://license.coscl.org.cn/MulanPubL-2.0
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PubL v2 for more details.
 */
package cool.taomu.compress

import cool.taomu.compress.impl.GZipCompress
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream
import java.util.zip.GZIPOutputStream
import org.apache.commons.io.IOUtils

class GzUtils {
	
	def static byte[] zip(byte[] arg) {
		var baos = new ByteArrayOutputStream();
		var byte[] result = newByteArrayOfSize(0);
		try(var gzip = new GZipCompress()){
			var out = gzip.compression(baos) as GZIPOutputStream;
			out.write(arg);
			out.finish;
			result = baos.toByteArray;
		}
		return result;
	}

	def static byte[] unzip(byte[] arg) {
		var bais = new ByteArrayInputStream(arg);
		try(var gzip = new GZipCompress()){
			var input = gzip.decompression(bais);
			return IOUtils.toByteArray(input);
		}
	}
	
	def static void main(String[] args){
		//println(new String(GzUtils.unzip(GzUtils.zip("Hello World".bytes)),"UTF-8"));
		//TarGzUtilsOld.zcvf("/Users/rcmu/workspace/gradle.properties","/Users/rcmu/a.tar.gz")
		TarGzUtils.zcvf("/Users/rcmu/workspace/gradle.properties","/Users/rcmu/a.tar.gz")
	}
}
