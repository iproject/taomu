package cool.taomu.runtime.groovy

import com.google.auto.service.AutoService
import cool.taomu.core.entity.TaskEntity.ScriptType
import cool.taomu.core.lang.AbsWrapperClassLoader
import cool.taomu.core.lang.WrapperClassLoader

@AutoService(AbsWrapperClassLoader)
@WrapperClassLoader(ScriptType.Groovy)
class GroovyWrapperClassloader extends AbsWrapperClassLoader {

	override load(String arg,ClassLoader loader) {
		var gs = new GroovyScript();
		gs.loaderScript(arg, loader);
		wrapper(gs)
	}
}
