/**
 * Copyright (c) 2023 murenchao
 * taomu is licensed under Mulan PubL v2.
 * You can use this software according to the terms and conditions of the Mulan PubL v2.
 * You may obtain a copy of Mulan PubL v2 at:
 *       http://license.coscl.org.cn/MulanPubL-2.0
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PubL v2 for more details.
 */
package cool.taomu.asm

import cool.taomu.asm.entity.ClassEntity
import cool.taomu.asm.entity.FieldEntity
import cool.taomu.asm.entity.MethodEntity
import java.io.PrintWriter
import java.lang.reflect.Constructor
import java.util.concurrent.ConcurrentSkipListSet
import org.objectweb.asm.ClassVisitor
import org.objectweb.asm.ClassWriter
import org.objectweb.asm.Opcodes
import org.objectweb.asm.Type
import org.objectweb.asm.commons.GeneratorAdapter
import org.objectweb.asm.commons.Method
import org.objectweb.asm.util.TraceClassVisitor
import org.slf4j.LoggerFactory

class CreateClass extends ClassLoader implements Comparable{
	val static LOG = LoggerFactory.getLogger(CreateClass);
	val static ccloader = new ConcurrentSkipListSet<ClassLoader>()
	ClassWriter cwriter;
	ClassVisitor tcvisitor;
	GeneratorAdapter ga;
	ClassEntity classEntity;

	new(ClassEntity classEntity, int version, int access) {
		this(classEntity, version, access, false);
	}

	new(ClassEntity classEntity, int version, int access, boolean isDebug) {
		ccloader.add(this);
		cwriter = new ClassWriter(ClassWriter.COMPUTE_MAXS + ClassWriter.COMPUTE_FRAMES);
		if (isDebug) {
			tcvisitor = new TraceClassVisitor(cwriter, new PrintWriter(System.err))
		} else {
			tcvisitor = cwriter;
		}
		this.classEntity = classEntity;
		tcvisitor.visit(version, access, classEntity.fullName, null, classEntity.superclass, classEntity.interfaces);
	}

	def static Class(ClassEntity classEntity, int version, boolean isDebug) {
		return new CreateClass(classEntity, version, Opcodes.ACC_PUBLIC, isDebug);
	}

	def static Class(ClassEntity classEntity, int version) {
		return new CreateClass(classEntity, version, Opcodes.ACC_PUBLIC);
	}

	def static Interface(ClassEntity classEntity, int version) {
		return new CreateClass(classEntity, version, Opcodes.ACC_PUBLIC + Opcodes.ACC_ABSTRACT + Opcodes.ACC_INTERFACE);
	}

	def static Abstract(ClassEntity classEntity, int version) {
		return new CreateClass(classEntity, version, Opcodes.ACC_PUBLIC + Opcodes.ACC_ABSTRACT);
	}

	def filed(FieldEntity field) {
		tcvisitor.visitField(field.access, field.name, field.descriptor, null, field.value).visitEnd();
	}

	def constructor(Constructor<?>[] constructor) {
		constructor.filterNull.forEach [
			var m = Method.getMethod(it);
			var ga = new GeneratorAdapter(Opcodes.ACC_PUBLIC, m, null, null, tcvisitor);
			ga.loadThis();
			if (it.parameters.size > 0) {
				ga.loadArgs(0, it.parameters.size - 1);
			}
			ga.invokeConstructor(Type.getObjectType(classEntity.superclass), m);
			ga.returnValue();
			ga.endMethod();
		]
		return this;
	}

	def method(MethodEntity method) {
		ga = new GeneratorAdapter(method.access, Method.getMethod(method.defineMethod, method.defaultPackage), null,
			null, tcvisitor);
		return new CreateMethod(this.classEntity, ga);
	}

	def end() {
		tcvisitor.visitEnd();
		return this;
	}

	def toByteArray() {
		return cwriter.toByteArray();
	}

	override findClass(String name) {
		var bytes = toByteArray();
		LOG.info("findClass");
		return super.defineClass(name, bytes, 0, bytes.length);
	}
	
	def static loadZlass(String name){
		return ccloader.findFirst[it.loadClass(name) !== null].loadClass(name);
	}
	
	override compareTo(Object arg0) {
		return 1;
	}
	
}
