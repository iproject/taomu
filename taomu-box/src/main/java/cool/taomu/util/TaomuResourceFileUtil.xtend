package cool.taomu.util

import java.io.File
import java.io.FileInputStream
import java.io.InputStream

class TaomuResourceFileUtil {
	def static InputStream read(String path) {
		var cl = Thread.currentThread.contextClassLoader;
		if (cl === null) {
			cl = ClassLoader.systemClassLoader;
		}
		if (cl !== null) {
			var is = cl.getResourceAsStream(path);
			if (is === null) {
				is = ClassLoader.getSystemResourceAsStream(path)
			}
			if (is === null) {
				is = new FileInputStream(new File("./config/" + path));
			}
			return is;
		}
	}
}
