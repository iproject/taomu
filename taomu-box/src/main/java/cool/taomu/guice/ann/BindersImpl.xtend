package cool.taomu.guice.ann

import com.google.inject.Module
import java.io.Serializable
import org.eclipse.xtend.lib.annotations.EqualsHashCode
import org.eclipse.xtend.lib.annotations.ToString
import org.eclipse.xtend.lib.annotations.Accessors

@EqualsHashCode
@ToString
@Accessors
class BindersImpl implements Binders, Serializable {

	String name;
	Binder[] value;
	String properties;
	Class<? extends Module>[] installs;
	String[] dependence;
	Binders[] dependenceBind;

	new(String name, Binder[] value) {
		this.name = name;
		this.value = value
	}

	override annotationType() {
		return Binders;
	}

	override name() {
		return this.name;
	}

	override value() {
		return this.value;
	}
	override properties(){
		return this.properties;
	}
	
	override installs() {
		return this.installs;
	}
	override dependence(){
		return this.dependence;
	}

}
