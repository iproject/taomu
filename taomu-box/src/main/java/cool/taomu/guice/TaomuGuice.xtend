/**
 * Copyright (c) 2023 murenchao
 * taomu is licensed under Mulan PubL v2.
 * You can use this software according to the terms and conditions of the Mulan PubL v2.
 * You may obtain a copy of Mulan PubL v2 at:
 *       http://license.coscl.org.cn/MulanPubL-2.0
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PubL v2 for more details.
 */
package cool.taomu.guice

import com.google.inject.AbstractModule
import com.google.inject.Binder
import com.google.inject.CreationException
import com.google.inject.Guice
import com.google.inject.Injector
import com.google.inject.Module
import cool.taomu.asm.CreateClass
import cool.taomu.asm.entity.ClassEntity
import cool.taomu.asm.entity.MethodEntity
import cool.taomu.guice.ann.Binders
import cool.taomu.guice.ann.BindersImpl
import cool.taomu.guice.ann.Box
import cool.taomu.guice.entity.BindersEntity
import cool.taomu.guice.entity.InterceptorEntity
import cool.taomu.util.TaomuGuiceUitl
import java.util.List
import java.util.UUID
import java.util.concurrent.ConcurrentSkipListMap
import org.objectweb.asm.Opcodes
import org.objectweb.asm.Type
import org.slf4j.LoggerFactory

class TaomuGuice {
	val static LOG = LoggerFactory.getLogger(TaomuGuice)
	val static cacheModule = new ConcurrentSkipListMap<String, Class<? extends Module>>();
	val static cacheBinders = new ConcurrentSkipListMap<String, BindersEntity>();

	def static Injector entry(Class<?> ... zlasses) throws CreationException{
		return entry("default", zlasses);
	}

	def static Injector entry(String name, Class<?> ... zlasses) throws CreationException{
		var module = new TaomuGuice().getModule(name, false, zlasses)
		return Guice.createInjector(module);
	}

	def static Injector entry(String name, List<InterceptorEntity> ir, Class<?> ... zlasses) throws CreationException{
		var tg = new TaomuGuice()
		return Guice.createInjector(tg.getModule(ir)).createChildInjector(tg.getModule(name, false, zlasses));
	}

	def getModule(List<InterceptorEntity> ir) {
		return new AbstractModule() {
			override void configure() {
				val binder = binder();
				ir.filterNull.forEach [
					binder.bindInterceptor(it.classMatcher, it.methodMatcher, it.interceptors)
				]
			}
		}
	}

	def Module getModule(String name, boolean isDebug, Class<?> ... zlasses) throws CreationException {
		if (cacheModule.containsKey(name)) {
			return cacheModule.get(name).newInstance;
		} else {
			zlasses.filterNull.forEach [ zlass |
				val box = zlass.getAnnotation(Box);
				val binders = zlass.getAnnotation(Binders);
				val binder = zlass.getAnnotation(cool.taomu.guice.ann.Binder);
				// 根据依赖名，组装依赖项
				if (box !== null) {
					box.value.forEach [
						LOG.info("box name:{}", it.name);
						if (!cacheBinders.containsKey(it.name)) {
							cacheBinders.put(it.name, TaomuGuiceUitl.toBinders(it));
						}
					]
				} else if (binders !== null) {
					LOG.info("name:{}", binders.name);
					if (!cacheBinders.containsKey(binders.name)) {
						cacheBinders.put(binders.name, TaomuGuiceUitl.toBinders(binders));
					}
				} else if (binder !== null) {
					if (!cacheBinders.containsKey("default")) {
						var tgb = TaomuGuiceUitl.toBinders(new BindersImpl("default", #[binder]));
						cacheBinders.put("default", tgb);
					}
				}
			]

			if (cacheBinders.size > 0) {
				cacheBinders.values.forEach [
					if (!cacheModule.containsKey(it.name)) {
						LOG.info("build :{}", it.name);
						cacheModule.put(it.name, buildModule(it, isDebug));
					}
				]
			}
			LOG.info("key:{}", name);
			return cacheModule.get(name).newInstance;
		// var injector = Guice.createInjector(module.newInstance);
		// return cacheInjector.getOrDefault(name, injector);
		}
	}

	def static Class<? extends Module> buildModule(BindersEntity binders, boolean isDebug) {
		if (binders !== null) {
			var name = UUID.randomUUID.toString.replace("-", "");
			var moduleName = #["Taomu", name, "Module"].join("");
			var classEntity = new ClassEntity("cool.taomu.guice", moduleName);
			classEntity.superclass = Type.getType(AbstractModule).internalName;
			var cc = CreateClass.Class(classEntity, Opcodes.V1_8, isDebug);
			cc.constructor(AbstractModule.constructors);
			var methodEntity = new MethodEntity("void configure()");
			var binder = new MethodEntity("com/google/inject/Binder binder()", true);
			val cm = cc.method(methodEntity).This.invokeVirtual(binder).store("binder", Binder)
			if (!binders.properties.nullOrEmpty) {
				cm.load("binder");
				cm.ldc(binders.properties);
				cm.invokeStatic(
					new MethodEntity("cool/taomu/util/PropertyUtils",
						"java/util/Properties load(java/lang/String)", true));
				cm.invokeStatic(
					new MethodEntity("com/google/inject/name/Names",
						"void bindProperties(com/google/inject/Binder,java/util/Properties)", true));
			}
			if (binders.dependence !== null && binders.dependence.size > 0) {
				binders.dependence.forEach [
					LOG.info("dependence");
					// TODO 可以测试了
					if (cacheModule.containsKey(it)) {
						var m = cacheModule.get(it);
						cm.ldc(m.name);
						cm.invokeStatic(
							new MethodEntity("cool/taomu/asm/CreateClass",
								"java/lang/Class loadZlass(java/lang/String)", true));
						cm.invokeVirtual(new MethodEntity("java/lang/Class", "java/lang/Object newInstance()", true));
						cm.store("cmodule", Object)
						cm.load("binder")
						cm.load("cmodule")
						cm.checkCast(Module)
						cm.invokeInterface(
							new MethodEntity("com/google/inject/Binder", "void install(com/google/inject/Module)",
								true));
					} else if (cacheBinders.containsKey(it)) {
						var bs = cacheBinders.get(it);
						cacheModule.put(bs.name, buildModule(bs, isDebug));
						cm.ldc(bs.name);
						cm.invokeStatic(
							new MethodEntity("cool/taomu/asm/CreateClass",
								"java/lang/Class loadZlass(java/lang/String)", true));
						cm.invokeVirtual(new MethodEntity("java/lang/Class", "java/lang/Object newInstance()", true));
						cm.store("cmodule", Object)
						cm.load("binder")
						cm.load("cmodule")
						cm.checkCast(Module)
						cm.invokeInterface(
							new MethodEntity("com/google/inject/Binder", "void install(com/google/inject/Module)",
								true))
					}
				]
			}
			if (binders.installs !== null && binders.installs.size > 0) {
				binders.installs.forEach [
					cm.NEW(it);
					cm.dup();
					cm.invokeSpecial(new MethodEntity(it.name, "void <init>()"));
					cm.store(it.name, it);
					cm.load("binder")
					cm.load(it.name)
					cm.invokeInterface(
						new MethodEntity("com/google/inject/Binder", "void install(com/google/inject/Module)", true));
				]
			}
			BinderRule.rule(cm, binders.values);
			cm.returnValue.endMethod;
			cc.end;
			LOG.info("end");
			return cc.loadClass(#["cool.taomu.guice", moduleName].join(".")) as Class<Module>;
		}
	}

}
