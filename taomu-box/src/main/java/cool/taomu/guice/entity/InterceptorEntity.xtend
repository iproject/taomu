package cool.taomu.guice.entity

import com.google.inject.matcher.Matcher
import java.lang.reflect.Method
import org.aopalliance.intercept.MethodInterceptor
import org.eclipse.xtend.lib.annotations.Accessors

@Accessors
class InterceptorEntity {
	Matcher<? super Class<?>> classMatcher;
	Matcher<? super Method> methodMatcher;
	MethodInterceptor[] interceptors;

	new() {
	}

	new(Matcher<? super Class<?>> classMatcher, Matcher<? super Method> methodMatcher,
		MethodInterceptor ... interceptors) {
		this.classMatcher = classMatcher;
		this.methodMatcher = methodMatcher;
		this.interceptors = interceptors;
	}
}
