package cool.taomu.guice.entity

import java.util.List
import org.eclipse.xtend.lib.annotations.Accessors

@Accessors
class BoxEntity {
	List<BindersEntity> values;

	new(List<BindersEntity> values) {
		this.values = values;
	}
}
