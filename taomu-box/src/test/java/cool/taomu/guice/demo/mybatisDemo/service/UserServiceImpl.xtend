package cool.taomu.guice.demo.mybatisDemo.service

import cool.taomu.guice.demo.mybatisDemo.entity.UserMapper
import javax.inject.Inject

class UserServiceImpl implements IUserService {
	
	@Inject
	UserMapper userMapper;
	
	override getUser() {
		return userMapper.users;
	}
	
	override getUserByName(String userName) {
		return userMapper.getUserByName(userName);
	}
	
	override getUserByAge(int age) {
		return userMapper.getUserByAge(age);
	}
	
}

class AuthServiceImpl implements IAuthService {
	
	@Inject
	UserMapper userMapper;
	
	override getUser() {
		return userMapper.users;
	}
	
	override getUserByName(String userName) {
		return userMapper.getUserByName(userName);
	}
	
	override getUserByAge(int age) {
		return userMapper.getUserByAge(age);
	}
	
}