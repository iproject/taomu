package cool.taomu.guice.demo.mybatisDemo.module

import com.google.inject.AbstractModule
import com.google.inject.matcher.Matchers
import com.google.inject.name.Names
import cool.taomu.guice.demo.mybatisDemo.service.IUserService
import cool.taomu.guice.demo.mybatisDemo.service.UserServiceImpl
import cool.taomu.guice.test.AopInterceptor
import cool.taomu.guice.test.GET
import cool.taomu.util.PropertyUtils

class DefaultModule extends AbstractModule{
	
	override void configure() {
		var binder = binder();
	}
}

class ServiceModule extends AbstractModule {
	override void configure() {
		var binder = binder();
		binder.bind(IUserService).to(UserServiceImpl);
	}
}
