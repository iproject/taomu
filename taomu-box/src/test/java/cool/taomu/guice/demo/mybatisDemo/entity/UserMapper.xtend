package cool.taomu.guice.demo.mybatisDemo.entity

import java.util.List
import org.apache.ibatis.annotations.Param
import org.apache.ibatis.annotations.Select
import org.eclipse.xtend.lib.annotations.Accessors

@Accessors
class User{
	String userName;
	int age;
}

interface UserMapper {
	
    @Select("SELECT * FROM user WHERE user_name = #{userName}")
    def User getUserByName(@Param("userName") String userName);

    @Select("SELECT * FROM user WHERE age = #{arg}")
    def User getUserByAge(int age);

    @Select("SELECT * FROM user ")
    def List<User> getUsers();
}
