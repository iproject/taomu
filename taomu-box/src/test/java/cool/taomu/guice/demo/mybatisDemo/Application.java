package cool.taomu.guice.demo.mybatisDemo;

import com.google.inject.Injector;

import cool.taomu.guice.TaomuGuice;
import cool.taomu.guice.ann.Binder;
import cool.taomu.guice.ann.Binders;
import cool.taomu.guice.demo.mybatisDemo.module.MybaitsModule;
import cool.taomu.guice.demo.mybatisDemo.module.ServiceModule;
import cool.taomu.guice.demo.mybatisDemo.service.AuthServiceImpl;
import cool.taomu.guice.demo.mybatisDemo.service.IAuthService;
import cool.taomu.guice.demo.mybatisDemo.service.IUserService;
import cool.taomu.guice.demo.mybatisDemo.service.UserServiceImpl;

@Binders(dependence = { "AuthService" }, installs = { ServiceModule.class,
		MybaitsModule.class }, properties = "h2.properties")
@Binders(name = "AuthService", value = { @Binder(bind = IAuthService.class, to = AuthServiceImpl.class) })
public class Application {
	public static void main(String[] args) {
		// Injector inj = Guice.createInjector(new DefaultModule());
		Injector inj = TaomuGuice.entry(Application.class);
		IUserService ius = inj.getInstance(UserServiceImpl.class);
		ius.getUser();
	}
}
